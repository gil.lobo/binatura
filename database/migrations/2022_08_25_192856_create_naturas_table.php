<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNaturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('naturas', function (Blueprint $table) {
            $table->id();
            $table->integer('codigo_lider');
            $table->string('nombre_lider');
            $table->string('estatus');
            $table->string('momento_carrera_inicial');
            $table->string('momento_carrera_final');
            $table->string('momento_carrera_real');
            $table->string('periodo_antencion');
            $table->integer('codigo_lider_inmediato');
            $table->string('lider_inmediata');
            $table->integer('total_lideres');
            $table->integer('disponibles');
            $table->integer('saldo_disponibles');
            $table->integer('activas');
            $table->integer('inactividad1');
            $table->integer('inactividad2');
            $table->integer('inactividad3');
            $table->integer('inactividad4');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('naturas');
    }
}
