<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMorosidadToNaturaGrupos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('natura_grupos', function (Blueprint $table) {
            $table->double('morosidad', 15, 8)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('natura_grupos', function (Blueprint $table) {
            $table->double('morosidad', 15, 8)->nullable();

        });
    }
}
