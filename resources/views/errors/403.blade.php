@extends('layouts.app')

@section('title', 'Terlarang')
@section('code', '403')

@section('content')
<div class="page-error mt-5">
  <div class="page-inner">
    <h1>403</h1>
    <div class="page-description">
      OOps! No tienes permiso para acceder a esta página web.
    </div>
    <div class="page-search">
      <div class="mt-2">
        <a href="{{ route('welcome') }}">Regresar</a>
      </div>
    </div>
  </div>
</div>
@endsection