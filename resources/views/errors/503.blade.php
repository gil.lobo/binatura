@extends('layouts.app')

@section('title', 'No Disponible')
@section('code', '503')

@section('content')
<div class="page-error mt-5">
  <div class="page-inner">
    <h1>503</h1>
    <div class="page-description">
      Servicio no disponible, tenga paciencia, actualmente estamos realizando el mantenimiento del sitio web. Póngase en contacto con el administrador.
    </div>
    <div class="page-search">
      <div class="mt-2">

      </div>
    </div>
  </div>
</div>
@endsection