@extends('layouts.app')

@section('title', 'Tidak resmi')
@section('code', '401')

@section('content')
<div class="page-error mt-5">
  <div class="page-inner">
    <h1>401</h1>
    <div class="page-description">
      error al ingresar el ID de usuario y la contraseña que hace que las credenciales no sean válidas por algún motivo
      cierto.
    </div>
    <div class="page-search">
      <div class="mt-2">
        <a href="{{ route('welcome') }}">Regresar al Inicio</a>
      </div>
    </div>
  </div>
</div>
@endsection