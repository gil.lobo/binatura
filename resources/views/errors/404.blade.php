@extends('layouts.app')

@section('title', 'Tidak Ditemukan')
@section('code', '404')

@section('content')
<div class="page-error mt-5">
  <div class="page-inner">
    <h1>404</h1>
    <div class="page-description">
      OOps! No se encontró la página que busca.
    </div>
    <div class="page-search">
      <div class="mt-2">
        <a href="{{ route('welcome') }}">Regresar al inicio</a>
      </div>
    </div>
  </div>
</div>
@endsection