@extends('layouts.backend')

@section('title', 'Dashboard')

@section('content')
<div class="card">
    <div class="card-header">
        <b style="font-size: 16px;" class="text text-muted">Información </b>
    </div>
    <div class="card-body">
        @if (isset($natura))
        <h5><b>Último Ciclo Cargado:</b> {{ $natura->ciclo ?? 'Aún no se ha cargado información' }} </h5><br>
        @if($naturaGpo->periodo_antencion == 'INICIA')
        <div class="alert alert-warning alert-has-icon">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <div class="alert-title">ATENCIÓN <br />
                En este momento está apunto de entrar a periodo de atención.
                <a href="{{route('dashboard.show', $natura->codigo_lider)}}"><b>Ver detalles</b></a>
                </div>
                
            </div>
        </div>
        @elseif($naturaGpo->periodo_antencion == 'SÍ')
        <div class="alert alert-danger alert-has-icon">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <div class="alert-title">ATENCIÓN <br>
                En este momento se encuentra en periodo de atención.
                <a href="{{route('dashboard.show', $natura->codigo_lider)}}">Ver detalles</a>
                </div>
                
            </div>
        </div>
        @endif
        @if ($natura->momento_carrera_inicial == 'CNE1')
                @if (($naturaGpo->disponibles-($naturaGpo->total_lideres-1)) < 3)
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">ATENCIÓN <br />
                            CN1 te pide 3 consultores. <br />Actualmente cuentas con {{ $naturaGpo->disponibles }} Consultores. Te faltan {{ (3 - ($naturaGpo->disponibles-($naturaGpo->total_lideres-1))) }} inicios o reinicios. !EVITA PERDER MÁS CONSULTORES!
                        
                            </div>
                        </div>
                    </div> 
                @endif
            @endif
            @if ($natura->momento_carrera_inicial == 'CNE2')
                @if (($naturaGpo->disponibles-($naturaGpo->total_lideres-1)) < 10)
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">ATENCIÓN <br />
                            CN2 te pide 10 consultores. <br />Actualmente cuentas con {{ $naturaGpo->disponibles }} Consultores. Te faltan {{ (10 - ($naturaGpo->disponibles-($naturaGpo->total_lideres-1))) }} inicios o reinicios. !EVITA PERDER MÁS CONSULTORES!
                            </div>
                        </div>
                    </div> 
                @endif
            @endif
            @if ($natura->momento_carrera_inicial == 'FN1')
                @if (($naturaGpo->disponibles-($naturaGpo->total_lideres-1)) < 15)
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">ATENCIÓN <br>
                            FN1 te pide 15 consultores. <br />Actualmente cuentas con {{ $naturaGpo->disponibles }} Consultores. Si restamos tus {{ $naturaGpo->total_lideres }} lideres tienes un total de {{ $naturaGpo->disponibles - $naturaGpo->total_lideres }} Consultores. Te faltan {{ (15 - ($naturaGpo->disponibles-($naturaGpo->total_lideres))) }} inicios o reinicios
                            
                            </div>
                        </div>
                    </div> 
               {{-- @elseif ($lideresCNE1 < 0 || $lideresCNE2 < 0 || $lideresFN1 < 0)   
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">Atención</div>
                            No se ha detectado personas con el nivel necesario.
                            Actualmente cuentas con {{$lideresCNE1}} CNE1 
                        </div>
                    </div> --}} 
                @endif  
            @endif
            @if ($natura->momento_carrera_inicial == 'FN2')
                @if (($naturaGpo->disponibles-($naturaGpo->total_lideres-1)) < 25)
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">ATENCIÓN <br> 
                                FN2 te pide 25 consultores. <br />Actualmente cuentas con {{ $naturaGpo->disponibles }} Consultores. Si restamos tus {{ $naturaGpo->total_lideres }} lideres tienes un total de {{ $naturaGpo->disponibles - $naturaGpo->total_lideres }} Consultores. Te faltan {{ (25 - ($naturaGpo->disponibles-($naturaGpo->total_lideres))) }} inicios o reinicios
                            </div>
                                
                        </div>
                    </div> 
                @endif
               {{--   @if ($lideresCNE2 < 1 || $lideresFN1 < 1 )
                    @if($lideresCNE2 == 0 && $lideresFN1 < 2)
                        <div class="alert alert-danger alert-has-icon col-12" >
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Atención</div>
                                No se ha detectado personas con el nivel necesario.
                                Actualmente cuentas con {{$lideresCNE2}} CNE2 y {{$lideresFN1}} FN1
                            </div>
                        </div>
                    @elseif ($lideresCNE2 == 0 && $lideresFN1 == 1 && $lideresF2 < 0)
                        <div class="alert alert-danger alert-has-icon col-12" >
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Atención</div>
                                No se ha detectado personas con el nivel necesario.
                                Actualmente cuentas con {{$lideresCNE2}} CNE2 y {{$lideresFN1}} FN1
                            </div>
                        </div>
                    
                    @elseif ($lideresCNE2 == 0 && $lideresFN1 > 0 && $lideresF2 > 0 && $lideresFN3 < 0)
                        <div class="alert alert-danger alert-has-icon col-12" >
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Atención</div>
                                No se ha detectado personas con el nivel necesario.
                                Actualmente cuentas con {{$lideresCNE2}} CNE2 y {{$lideresFN1}} FN1
                            </div>
                        </div>
                    @endif
                @endif--}}
            @endif
            
            @if ($natura->momento_carrera_inicial == 'FN3')
                @if(($naturaGpo->disponibles-($naturaGpo->total_lideres-1)) < 35)
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">ATENCIÓN <br>
                            FN3 te pide 35 consultores. <br />Actualmente cuentas con {{ $naturaGpo->disponibles }} Consultores. Si restamos tus {{ $naturaGpo->total_lideres }} lideres tienes un total de {{ $naturaGpo->disponibles - $naturaGpo->total_lideres }} Consultores. Te faltan {{ (35 - ($naturaGpo->disponibles-($naturaGpo->total_lideres))) }} inicios o reinicios
                            
                        </div></div>
                    </div>
                @endif
                {{--  @if ($lideresCNE2 < 1 || $lideresFN1 < 1 || $lideresFN2 < 1)
                    @if ($lideresCNE2 == 0 && $lideresFN1 == 0 && $lideresFN2 < 2)
                        <div class="alert alert-danger alert-has-icon col-12" >
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Atención</div>
                                No se ha detectado personas con el nivel necesario.
                                Actualmente cuentas con {{$lideresCNE2}} CNE2 y {{$lideresFN1}} FN1 y {{$lideresFN2}} FN2
                            </div>
                        </div>                        
                    @endif
                    @if ($lideresCNE2 == 0 && $lideresFN1 == 1 && $lideresFN2 == 0 && $lideresFN3 < 0)
                        <div class="alert alert-danger alert-has-icon col-12" >
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Atención</div>
                                No se ha detectado personas con el nivel necesario.
                                Actualmente cuentas con {{$lideresCNE2}} CNE2 y {{$lideresFN1}} FN1 y {{$lideresFN2}} FN2
                            </div>
                        </div>                        
                    @endif
                    @if ($lideresCNE2 == 1 && $lideresFN1 == 0 && $lideresFN2 == 0 && $lideresFN3 < 2)
                        <div class="alert alert-danger alert-has-icon col-12" >
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Atención</div>
                                No se ha detectado personas con el nivel necesario.
                                Actualmente cuentas con {{$lideresCNE2}} CNE2 y {{$lideresFN1}} FN1 y {{$lideresFN2}} FN2
                            </div>
                        </div>                        
                    @endif
                       
                @endif--}}
            @endif
            @if ( $natura->momento_carrera_inicial == 'TN1' )
                @if ( ( $naturaGpo->disponibles - ( $naturaGpo->total_lideres-1 ) ) < 45 )
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">ATENCIÓN <br />
                            TN1 te pide 45 consultores. <br />Actualmente cuentas con {{ $naturaGpo->disponibles }} Consultores. Si restamos tus {{ $naturaGpo->total_lideres }} lideres tienes un total de {{ $naturaGpo->disponibles - $naturaGpo->total_lideres }} Consultores. Te faltan {{ (45 - ($naturaGpo->disponibles-($naturaGpo->total_lideres))) }} inicios o reinicios
                            
                            </div>
                        </div>
                    </div> 
                {{--  @elseif ( $lideresCNE2 < 1 || $lideresFN1 < 2 || $lideresFN2 < 1 || $lideresFN3 < 1)
                    @if ($lideresCNE2 == 0 && $lideresFN1 == 0 && $lideresFN2 < 2 && $lideresFN3 < 1 && $lideresTN1 < 1)
                        <div class="alert alert-danger alert-has-icon col-12" >
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Atención</div>
                                No se ha detectado personas con el nivel necesario.
                                Actualmente cuentas con {{$lideresCNE2}} CNE2 y {{$lideresFN1}} FN1 y {{$lideresFN2}} FN2
                            </div>
                        </div>                        
                    @endif
                    @if ($lideresCNE2 == 0 && $lideresFN1 == 0 && $lideresFN2 < 2 && $lideresFN3 < 1 && $lideresTN1 < 1)
                        <div class="alert alert-danger alert-has-icon col-12" >
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Atención</div>
                                No se ha detectado personas con el nivel necesario.
                                Actualmente cuentas con {{$lideresCNE2}} CNE2 y {{$lideresFN1}} FN1 y {{$lideresFN2}} FN2
                            </div>
                        </div>                        
                    @endif--}}
                @endif
            @endif
            @if ($natura->momento_carrera_inicial == 'TN2')
                @if (($naturaGpo->disponibles-($naturaGpo->total_lideres-1)) < 55)
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">ATENCIÓN <br>
                            TN2 te pide 55 consultores. <br />Actualmente cuentas con {{ $naturaGpo->disponibles }} Consultores. Si restamos tus {{ $naturaGpo->total_lideres }} lideres tienes un total de {{ $naturaGpo->disponibles - $naturaGpo->total_lideres }} Consultores. Te faltan {{ (55 - ($naturaGpo->disponibles-($naturaGpo->total_lideres))) }} inicios o reinicios
                            </div>
                        </div>
                    </div> 
                @endif
            @endif
            @if ($natura->momento_carrera_inicial == 'TN3')
                @if (($naturaGpo->disponibles-($naturaGpo->total_lideres-1)) < 65)
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">ATENCIÓN <br>
                            TN3 te pide 65 consultores. <br />Actualmente cuentas con {{ $naturaGpo->disponibles }} Consultores. Si restamos tus {{ $naturaGpo->total_lideres }} lideres tienes un total de {{ $naturaGpo->disponibles - $naturaGpo->total_lideres }} Consultores. Te faltan {{ (65 - ($naturaGpo->disponibles-($naturaGpo->total_lideres))) }} inicios o reinicios
                            
                            </div>
                        </div>
                    </div> 
                @endif
            @endif
            {{-- @if ($natura->momento_carrera_inicial == 'TN3')
                @if ($lideresFN2 < 3 || $lideresFN3 < 2 || $lideresTN1 < 1 || $lideresTN2 < 1 || ($naturaGpo->disponibles-($naturaGpo->total_lideres-1)) < 65)
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">Atención</div>
                            No se ha detectado personas con el nivel necesario.
                            Actualmente cuentas con {{$lideresFN2}} FN2 y {{$lideresFN3}} FN3 y {{$lideresTN1}} TN1 y {{$lideresTN2}} TN2 y {{ ($naturaGpo->disponibles-($naturaGpo->total_lideres-1)) }} Consultores
                        </div>
                    </div> 
                @endif
            @endif --}}
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-info">
                        <i class="fas fa-running"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">{{$natura->momento_carrera_inicial}}</b>
                        </div>
                        <div class="card-body">
                             {{$natura->codigo_lider}} .- {{$natura->nombre_lider}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-info">
                        <i class="fas fa-running"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Momento de Carrera Inicial</b>
                        </div>
                        <div class="card-body">
                            {{$natura->momento_carrera_inicial}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-running"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Momento de Carrera Real</b>
                        </div>
                        <div class="card-body">
                            {{$natura->momento_carrera_real}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-running"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Momento de Carrera Final</b>
                        </div>
                        <div class="card-body">
                            {{$natura->momento_carrera_final}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Líderes en CNE1</b>
                        </div>
                        <div class="card-body">
                            {{$lideresCNE1}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Líderes en CNE2</b>
                        </div>
                        <div class="card-body">
                            {{$lideresCNE2}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-info">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Líderes en FN1</b>
                        </div>
                        <div class="card-body">
                            {{$lideresFN1}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Líderes en FN2</b>
                        </div>
                        <div class="card-body">
                            {{$lideresFN2}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Líderes en FN3</b>
                        </div>
                        <div class="card-body">
                            {{$lideresFN3}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Líderes en TN1</b>
                        </div>
                        <div class="card-body">
                            {{$lideresTN1}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-info">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Líderes en TN2</b>
                        </div>
                        <div class="card-body">
                            {{$lideresTN2}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Líderes en TN3</b>
                        </div>
                        <div class="card-body">
                            {{$lideresTN3}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1" >
                    <div class="card-icon" style="background:#8F33FF; ">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px; color:#8F33FF;" class="text" >Aniversario como líder</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->aniversario2}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (isset($naturaGpo))
        <div class="row col-md-12">
            <b style="font-size: 16px;" class="text text-muted">Grupo Directo</b>
            @if($naturaGpo->estatus == 'Inactiva 2')
                <div class="alert alert-danger alert-has-icon col-lg-12 col-md-12 col-sm-12 col-12" >
                    <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                    <div class="alert-body">
                        <div class="alert-title">Alerta Máxima <br />Realiza URGENTE tu pedido o caeras a Consultor(a)</div>   
                    </div>
                </div> 
            @endif
        </div>
        <div class="row">

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon @if($naturaGpo->estatus != 'Activo') bg-danger @else bg-success @endif">
                        <i class="fas fa-circle"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Usted se encuentra <br />{{$natura->estatus}}</b>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Disponibles</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->disponibles}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Saldo disponibles</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->saldo_disponibles}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Activas</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->activas}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-percentage"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Porcentaje de Actividad</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->actividad}} %
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-success">Total de líderes</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->total_lideres}}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Activas Deseadas</b>
                        </div>
                        <div class="card-body">
                            {{round($porscentaje)}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Activas @if($total < 0) Excedente @else Faltantes @endif</b>
                        </div>
                        <div class="card-body">
                            {{ abs ( round($total) ) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-percentage"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Porcentaje Ideal</b>
                        </div>
                        <div class="card-body">
                            {{ $ideal->ideal }} %


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-user-minus"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Deuda</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->morosidad}} %
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-eye"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">CPV</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->cpv}} %
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon" style="background:#FF9C33; ">
                        <i class="fas fa-sync-alt"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-warning">Ciclos como Líder</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->ciclo_inicio}} 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Inactiva 1</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->inactividad1}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Inactiva 2</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->inactividad2}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Inactiva 3</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->inactividad3}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Inactiva 4</b>
                        </div>
                        <div class="card-body">
                            {{$naturaGpo->inactividad4}}
                        </div>
                    </div>
                </div>
            </div>
            @if ($ideal->ideal <= $naturaGpo->actividad )
                <div class="col-12 col-sm-6 col-lg-12">

                    <div class="alert alert-success alert-dismissible show fade">

                        <div class="alert-body">
                            <button class="close" data-dismiss="alert">
                                <span>×</span>
                            </button>
                            <div class="alert-title">¡Felicidades! <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            Haz logrado los objetivos en este ciclo. Sigue así.

                        </div>
                    </div>
                </div>
                @elseif($naturaGpo->actividad < 60) <div class="col-12 col-sm-6 col-lg-12">
                    <div class="alert alert-warning alert-dismissible show fade">

                        <div class="alert-body">
                            <button class="close" data-dismiss="alert">
                                <span>×</span>
                            </button>
                            <div class="alert-title">¡Vamos! </i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            Por <b>CÓDIGO DE HONOR</b> el mínimo necesario es el 60% te faltan
                            {{ ceil(($naturaGpo->disponibles * .60) - $naturaGpo->activas) }} </div>
                        </div>
                    </div>
                    @else



                    @endif
                    <div class="col-12 col-sm-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                                        <b style="font-size: 20px;">Mapa de saldo por líder ✓</b>
                            </div>
                            <div class="card-body">
                                <div>
                                    @if($natura->momento_carrera_inicial == 'TN2')
                                <img class="img-fluid rounded" src="{{ asset('stisla/img/TN2.jpeg') }}">
                            
                            @elseif($natura->momento_carrera_inicial == 'FN1')
                                <img class="img-fluid rounded" src="{{ asset('stisla/img/FN1.jpeg') }}">
                            @elseif($natura->momento_carrera_inicial == 'FN2')
                                <img class="img-fluid rounded" src="{{ asset('stisla/img/FN2.jpeg') }}">

                            @elseif($natura->momento_carrera_inicial == 'FN3')
                                <img class="img-fluid rounded" src="{{ asset('stisla/img/FN3.jpeg') }}">

                            @elseif($natura->momento_carrera_inicial == 'TN1')
                                <img class="img-fluid rounded" src="{{ asset('stisla/img/TN1.jpeg') }}">

                            @elseif($natura->momento_carrera_inicial == 'TN3')
                                <img class="img-fluid rounded" src="{{ asset('stisla/img/TN3.jpeg') }}">

                            @elseif($natura->momento_carrera_inicial == 'IN')
                                <img class="img-fluid rounded" src="{{ asset('stisla/img/IN.jpeg') }}">

                            @elseif($natura->momento_carrera_inicial == 'AN')
                                <img class="img-fluid rounded" src="{{ asset('stisla/img/AN.jpeg') }}">

                            @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-success alert-dismissible show fade col-12 col-sm-12 col-lg-12">

                        <div class="alert-body">
                            <button class="close" data-dismiss="alert">
                                <span>×</span>
                            </button>
                            <div class="alert-title">
                            Para ser un SÚPER LÍDER llega al cuadrante SÚPER VERDE</div></div>
                        </div>
                    </div>

        </div>
        <div class="row">
            @if($natura->momento_carrera_inicial == 'TN2')
            @include('inclides.tn2')
            @elseif($natura->momento_carrera_inicial == 'CNE1')
            @include('inclides.cne1')
            @elseif($natura->momento_carrera_inicial == 'CNE2')
            @include('inclides.cne2')
            @elseif($natura->momento_carrera_inicial == 'FN1')
            @include('inclides.fn1')
            @elseif($natura->momento_carrera_inicial == 'FN2')
            @include('inclides.fn2')
            @elseif($natura->momento_carrera_inicial == 'FN3')
            @include('inclides.fn3')
            @elseif($natura->momento_carrera_inicial == 'TN1')
            @include('inclides.tn1')
            @elseif($natura->momento_carrera_inicial == 'TN3')
            @include('inclides.tn3')
            @elseif($natura->momento_carrera_inicial == 'IN')
            @include('inclides.in')
            @elseif($natura->momento_carrera_inicial == 'AN')
            @include('inclides.an')
            @endif
        </div>


        @else
        <p>Para mostrar la información es necesario cargar los exceles correspondientes.</p>
        @endif
        <b style="font-size: 16px;" class="text text-muted">RED</b>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Disponibles</b>
                        </div>
                        <div class="card-body">
                            {{$natura->disponibles}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Saldo disponibles</b>
                        </div>
                        <div class="card-body">
                            {{$natura->saldo_disponibles}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Activas</b>
                        </div>
                        <div class="card-body">
                            {{$natura->activas}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-percentage"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Porcentaje de Actividad</b>
                        </div>
                        <div class="card-body">
                            {{$natura->actividad}} %
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-success">Total de líderes</b>
                        </div>
                        <div class="card-body">
                            {{$natura->total_lideres}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon" style="background: #0FF964;">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Activas Deseadas</b>
                        </div>
                        <div class="card-body">
                            {{ceil($porscentajeRed)}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Activas @if($totalRed < 0) Excedente @else Faltantes @endif</b>
                        </div>
                        <div class="card-body">
                            {{ abs ( ceil($totalRed) ) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon" style="background: #0FF964;">
                        <i class="fas fa-percentage"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Porcentaje Ideal</b>
                        </div>
                        <div class="card-body">
                            {{ $ideal->ideal }} %

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-user-minus"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Deuda</b>
                        </div>
                        <div class="card-body">
                            {{$natura->morosidad}} %
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-eye"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">CPV</b>
                        </div>
                        <div class="card-body">
                            {{$natura->cpv}} %
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Saldo Súper Verde 0.3</b>
                        </div>
                        <div class="card-body">
                            {{ceil($natura->total_lideres * .3)}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon" style="background:#0F6604; ">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Saldo Súper Verde 0.5</b>
                        </div>
                        <div class="card-body">
                            {{ceil($natura->total_lideres * .5)}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Inactiva 1</b>
                        </div>
                        <div class="card-body">
                            {{$natura->inactividad1}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Inactiva 2</b>
                        </div>
                        <div class="card-body">
                            {{$natura->inactividad2}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Inactiva 3</b>
                        </div>
                        <div class="card-body">
                            {{$natura->inactividad3}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <b style="font-size: 16px;" class="text text-muted">Inactiva 4</b>
                        </div>
                        <div class="card-body">
                            {{$natura->inactividad4}}
                        </div>
                    </div>
                </div>
            </div>

            @if ($ideal->ideal <= $natura->actividad )
                <div class="col-12 col-sm-6 col-lg-12">

                    <div class="alert alert-success alert-dismissible show fade">

                        <div class="alert-body">
                            <button class="close" data-dismiss="alert">
                                <span>×</span>
                            </button>
                            <div class="alert-title">¡Felicidades! <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            Haz logrado los objetivos en este ciclo. Sigue así.

                        </div>
                    </div>
                </div>
                @elseif($natura->actividad < 60) <div class="col-12 col-sm-6 col-lg-12">
                    <div class="alert alert-warning alert-dismissible show fade">

                        <div class="alert-body">
                            <button class="close" data-dismiss="alert">
                                <span>×</span>
                            </button>
                            <div class="alert-title">¡Vamos! </i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            Aún puedes lograrlo ánimo. Para completar el mínimo necesario al 60% te faltan 
                            {{ ceil(($natura->disponibles * .60) - $natura->activas) }} </div>

                        </div>
                    </div>
        </div>
        @else

        @endif
        @else
        <p>Para mostrar la información es necesario cargar los exceles correspondientes.</p>
        @endif
        <br>

        @if (isset($lideres))
        <table class="table table-striped table-sm thead-light table-bordered dt-responsive nowrap " style="width:100%" id="reporte">
            <thead>
                <th>Líder</th>
                <th>Acciones</th>
            </thead>
            <tbody>
                @foreach($lideres as $lider)
                <tr>
                    <td>
                        {{$lider->nombre_lider}}
                    </td>
                    <td>
                        <a class="btn btn-info btn-sm" href="{{route('lider.show', $lider->codigo_lider)}}">Mostrar Información</a>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    </div>
</div>
@endsection
