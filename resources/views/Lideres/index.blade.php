@extends('layouts.backend')

@section('title', 'Dashboard')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Líderes</h4>
    </div>
    <div class="card-body">
        <table class="table table-striped table-sm thead-light table-bordered dt-responsive nowrap " style="width:100%" id="reporte">
            <thead>
                <th>Líder</th>
                <th>Acciones</th>
            </thead>
            <tbody>
            
                @foreach($lideres as $lider)
                    <tr>
                        <td>
                            {{$lider->nombre_lider}}
                        </td>
                        <td>
                            <a class="btn btn-info btn-sm" href="{{route('lider.show', $lider->codigo_lider)}}">Mostrar Información</a>
                                
                        </td>
                    </tr>
                @endforeach
          
            </tbody>
        </table>
        
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
   
$(function () {
    
    $('#reporte').DataTable({
        responsive:true,
        autoWidth:false,
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        
        },
    });
    
});
</script>
@endsection