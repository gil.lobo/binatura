<div>
    <!-- Sidebar outter -->
    <div class="main-sidebar sidebar-style-2">
        <!-- sidebar wrapper -->
        <aside id="sidebar-wrapper">
            <!-- sidebar brand -->
            <div class="sidebar-brand">
                <a href="{{ route('welcome') }}">{{ config('app.name', 'Natura') }}</a>
            </div>
            <!-- sidebar menu -->
            <ul class="sidebar-menu">
                <!-- menu header -->
                <li class="menu-header">General</li>
                <!-- menu item -->
                <li class="{{ Route::is('dashboard') ? 'active' : '' }}">
                    <a href="{{ route('dashboard') }}">
                        <i class="fas fa-fire"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                @can('Ver Excel Grupo')
                <li class="{{ Route::is('import-view-direct') ? 'active' : '' }}">
                    <a href="{{ route('import-view-direct') }}">
                        <i class="fas fa-file-excel"></i>
                        <span>Subir Excel Gpo. Directo</span>
                    </a>
                </li>
                @endcan
                @can('Ver Excel Red')
                <li class="{{ Route::is('import-view') ? 'active' : '' }}">
                    <a href="{{ route('import-view') }}">
                        <i class="fas fa-file-excel"></i>
                        <span>Subir Excel Red</span>
                    </a>
                </li>
                @endcan
                
                @can('Ver Lideres')
                <li class="{{ Route::is('lideres*') ? 'active' : '' }}">
                    <a href="{{ route('lideres') }}">
                        <i class="fas fa-users"></i>
                        <span>Líderes</span>
                    </a>
                </li>
                @endcan
                @can('Ver Configuracion')
                <li class="{{ Route::is('porcentaje*') ? 'active' : '' }}">
                    <a href="{{ route('porcentaje') }}">
                        <i class="fas fa-cog"></i>
                        <span>Configuración</span>
                    </a>
                </li>
                @endcan
                
                @can('ver usuarios')
                <li class="side-menus {{ Request::is('usuarios*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/admin/usuarios') }}" data-toggle="tooltip" data-placement="right" data-original-title="Usuarios">
                        <i class=" fas fa-users"></i><span>Usuarios</span>
                    </a>
                </li>
                @endcan
                @can('ver rol')
                <li class="side-menus {{ Request::is('roles*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/admin/roles') }}" data-toggle="tooltip" data-placement="right" data-original-title="Roles">
                        <i class=" fas fa-user-lock"></i><span>Roles</span>
                    </a>
                </li>
                @endcan
                <li class="{{ Route::is('profile') ? 'active' : '' }}">
                    <a href="{{ route('profile') }}">
                        <i class="fas fa-user"></i>
                        <span>Profile</span>
                    </a>
                </li>
            </ul>
        </aside>
    </div>
</div>
