<div class="col-lg-6 col-md-6 col-sm-12">
    <div class="card card-statistic-2">
        <div class="card-stats">
            <div class="card-stats-title">
                <div class="card-icon shadow-primary bg-primary">
                    <i class="fas fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <b>Yo estoy</b>
                    </div>
                    <div class="card-body">
                        FN1
                        <br>
                        <h5>Necesito</h5><b></b>
                    </div>
                </div>

                <div class="d-inline">

                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-md border border-success " style="border: 1px solid black;">
                    <tbody>
                        <tr>
                            <th class="bg-info text-white">CN</th>
                            <th class="bg-info text-white">CNE1</th>
                        </tr>
                        <tr>
                            <td>15</td>
                            <td>1</td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<div class="col-lg-6 col-md-6 col-sm-12">
    <div class="card card-statistic-2">
        <div class="card-stats">
            <div class="card-stats-title">
                <div class="card-icon shadow-primary bg-primary">
                    <i class="fas fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <b>Yo Voy</b>
                    </div>
                    <div class="card-body">
                        FN2
                        <br>
                        <h5>Necesito</h5><b></b>
                    </div>
                </div>

                <div class="d-inline">

                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-md border border-success " style="border: 1px solid black;">
                    <tbody>
                        <tr>
                            <th class="bg-primary text-white">CN</th>
                            <th class="bg-primary text-white">CNE2</th>
                            <th class="bg-primary text-white">FN1</th>
                        </tr>
                        <tr>
                            <td>25</td>
                            <td>1</td>
                            <td>1</td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
