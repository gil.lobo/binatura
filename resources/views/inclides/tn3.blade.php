<div class="col-lg-6 col-md-6 col-sm-12">
    <div class="card card-statistic-2">
        <div class="card-stats">
            <div class="card-stats-title">
                <div class="card-icon shadow-primary bg-primary">
                    <i class="fas fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <b>Yo estoy</b>
                    </div>
                    <div class="card-body">
                        TN3
                        <br>
                        <h5>Necesito</h5><b></b>
                    </div>
                </div>

                <div class="d-inline">

                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-md border border-success " style="border: 1px solid black;">
                    <tbody>
                        <tr>
                            <th style="background:#0F6604; " class="text-white">CN</th>
                            <th style="background:#0F6604;" class="text-white">FN2</th>
                            <th style="background:#0F6604;" class="text-white">FN3</th>
                            <th style="background:#0F6604;" class="text-white">TN1</th>
                            <th style="background:#0F6604;" class="text-white">TN2</th>
                        </tr>
                        <tr>
                            <td>65</td>
                            <td>3</td>
                            <td>2</td>
                            <td>1</td>
                            <td>1</td>
                        </tr>

                    </tbody>
                </table>
            </div>


        </div>

    </div>
</div>
<div class="col-lg-6 col-md-6 col-sm-12">
    <div class="card card-statistic-2">
        <div class="card-stats">
            <div class="card-stats-title">
                <div class="card-icon shadow-primary bg-primary">
                    <i class="fas fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <b>Yo Voy</b>
                    </div>
                    <div class="card-body">
                        IN
                        <br>
                        <h5>Necesito</h5><b></b>
                    </div>
                </div>

                <div class="d-inline">

                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-md border border-success " style="border: 1px solid black;">
                    <tbody>
                        <tr>
                            <th style="background:#ECF30E; " class="text-white">CN</th>
                            <th style="background:#ECF30E;" class="text-white">FN3</th>
                            <th style="background:#ECF30E;" class="text-white">TN1</th>
                            <th style="background:#ECF30E;" class="text-white">TN2</th>
                            <th style="background:#ECF30E;" class="text-white">TN3</th>
                        </tr>
                        <tr>
                            <td>75</td>
                            <td>3</td>
                            <td>3</td>
                            <td>2</td>
                            <td>1</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
