@extends('layouts.backend')

@section('title', 'Dashboard')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Detalle</h4>
    </div>
    <div class="card-body">
        
        <h5>Líderes en Periodo de Atención</h5>
            <div class="row">                
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-warning">
                            <i class="fas fa-circle"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>En inicia </h4>
                            </div>
                            <div class="card-body">
                                {{$INICIA}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-danger">
                            <i class="fas fa-circle"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>En periodo de atención </h4>
                            </div>
                            <div class="card-body">
                                {{$Si}}
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <h4>Grupo Directo</h4> 
            
            <table class="table table-striped table-sm thead-light table-bordered dt-responsive nowrap " style="width:100%" id="reporte2">
                <thead>
                    <th>Líder</th>
                    <th>Periodo de Atención</th>
                    <th>Momento Inicial</th>
                    <th>Momento Real</th>
                    <th>Momento Final</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                    @foreach($lideresIs as $lider)
                        <tr>
                            <td>
                            {{$lider->nombre_lider}}
                                
                            </td>
                            <td>    
                                <div  class="badge @if($lider->periodo_antencion == 'INICIA') badge-warning @elseif ($lider->periodo_antencion == 'SÍ') badge-danger @else badge-success @endif">{{$lider->periodo_antencion}}</div>
                            </td>
                            <td>
                                {{$lider->momento_carrera_inicial}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_real}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_final}}
                            </td>
                            <td>
                                <a class="btn btn-info btn-sm" href="{{route('lider.show', $lider->codigo_lider)}}"><i class="fas fa-eye"></i></a>                                    
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
    </div>
    
</div>

@endsection
@section('js')
<script type="text/javascript">
   
$(function () {
    


    $('#reporte').DataTable({
        responsive:true,
        autoWidth:false,
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        
        },
    });

    $('#reporte2').DataTable({
        responsive:true,
        autoWidth:false,
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        
        },
    });
    
});

</script>
@endsection