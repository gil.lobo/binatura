@extends('layouts.backend')

@section('title', 'Import')
@section('css')
<style>
    #loader {
    display: none;
 
}
</style>
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        @if (isset($natura))
            <h5><b>Último Ciclo Cargado:</b> {{ $natura->ciclo ?? 'Aún no se ha cargado información' }} {{date_format($natura->created_at,'Y') ?? '' }}</h5><br>
       
        @endif
       
        
    </div>
    <div class="card-body">
        <h4>Cargar Excel</h4>
        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data" id="fileUploadForm">
            @csrf
            <div class="row">
              @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
              @endif
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Example file input</label>
                        <input type="file" name="file" class="form-control-file" id="exampleFormControlFile1">
                      
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="ciclo">Seleccione el Ciclo</label>
                        <input type="number" name="ciclo" class="form-control" id="ciclo">
                      
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                </div>
            </div>
            <div id='loader' class="col-md-12"><img src="{{url('stisla/img/natura.png')}}"
                    alt="logo" width="100" class="shadow-light rounded-circle"> Por favor espere estamos porcesando su información....</div>
            <input type="submit" value="Subir Archivo" class="btn btn-primary">
        </form>
    </div>
</div>
@endsection

@section('js')
<script>

    $(function () {
        $(document).ready(function () {
            
            $('#fileUploadForm').ajaxForm({
                beforeSend: function () {
                    var percentage = '0';
                    $('#loader').show();
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentage = percentComplete;
                    $('.progress .progress-bar').css("width", percentage+'%', function() {
                      return $(this).attr("aria-valuenow", percentage) + "%";
                    })
                },
                error: function(data){
                     $('.progress .progress-bar').css("width", '0%', function() {
                      return $(this).attr("aria-valuenow", 0) + "%";
                    })
                    $('#loader').hide();
                     toastr.error("Debe completar la información para continuar");
                     console.log(data);
                },
                success: function (xhr) {
                    window.location.reload();
                }
            });
        });
    });
</script>
@endsection