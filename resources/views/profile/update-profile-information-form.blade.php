<div class="card">
    <form method="POST" action="{{ route('user-profile-information.update') }}">
        @csrf @method('PUT')

        <div class="card-header">
            <h4>Editar Perfil</h4>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label>{{ __('Nombre') }}</label>
                <input type="text" class="form-control" name="name" value="{{ old('name') ?? auth()->user()->name }}"
                    required autofocus autocomplete="name" />
            </div>
            <div class="form-group">
                <label>Correo Electrónico</label>
                <input type="email" name="email" class="form-control"
                    value="{{ old('email') ?? auth()->user()->email }}" required autofocus />
            </div>
            <div class="form-group">
                <label>Código de Líder</label>
                <input type="codigo" name="codigo" class="form-control"
                    value="{{ old('codigo') ?? auth()->user()->codigo }}" required autofocus />
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-primary" type="submit">
                {{ __('Actualizar Perfil') }}
            </button>
        </div>
    </form>
</div>