@extends('layouts.infoapp')

@section('title', 'Import')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Líder {{$natura->nombre_lider}}</h4>
    </div>
    <div class="card-body">
        <div class="row">
            @if($natura->periodo_antencion == 'INICIA') 
                <div class="alert alert-warning alert-has-icon">
                    <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                    <div class="alert-body">
                        <div class="alert-title">Atención</div>
                        En este momento está apunto de entrar a periodo de atención.
                        
                    </div>
                </div>
            @elseif($natura->periodo_antencion == 'Si')
                <div class="alert alert-danger alert-has-icon col-12" >
                    <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                    <div class="alert-body">
                        <div class="alert-title">Atención</div>
                        Se encuentra en Periodo de Atención.
                        
                    </div>
                </div>
            @endif

            @if ($natura->momento_carrera_inicial == 'FN1')
                @if ($lideresFN1 == 0)
                    <div class="alert alert-danger alert-has-icon col-12" >
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                            <div class="alert-title">Atención</div>
                            No se ha detectado personas con el nivel necesario.
                            Actualmente cuentas con {{$lideresFN1}} CNE1
                        </div>
                    </div> 
                @endif
            @elseif ($natura->momento_carrera_inicial == 'FN2')
            @if ($lideresFN21 < 1 || $lideresFN22 < 1)
                <div class="alert alert-danger alert-has-icon col-12" >
                    <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                    <div class="alert-body">
                        <div class="alert-title">Atención</div>
                        No se ha detectado personas con el nivel necesario.
                        Actualmente cuentas con {{$lideresFN21}} CNE2 Y {{$lideresFN22}} FN1
                    </div>
                </div> 
            @endif
                
            @endif
            
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-info">
                        <i class="fas fa-running"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Momento de Carrera Inicial</h4>
                        </div>
                        <div class="card-body">
                            {{$natura->momento_carrera_inicial}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-running"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Momento de Carrera Real</h4>
                        </div>                       
                        <div class="card-body">
                            {{$natura->momento_carrera_real}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-running"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Momento de Carrera Final</h4>
                        </div>
                        <div class="card-body">
                            {{$natura->momento_carrera_final}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total de líderes</h4>
                        </div>
                        <div class="card-body">
                            {{$natura->total_lideres}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Disponibles</h4>
                        </div>
                        <div class="card-body">
                            {{$natura->disponibles}}
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Saldo disponibles</h4>
                        </div>
                        <div class="card-body">
                            {{$natura->saldo_disponibles}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Activos</h4>
                        </div>
                        <div class="card-body">
                            {{$natura->activas}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Incia 1</h4>
                        </div>
                        <div class="card-body">
                            {{$natura->inactividad1}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Incia 2</h4>
                        </div>
                        <div class="card-body">
                            {{$natura->inactividad2}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Incia 3</h4>
                        </div>
                        <div class="card-body">
                            {{$natura->inactividad3}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Inicia 4</h4>
                        </div>
                        <div class="card-body">
                            {{$natura->inactividad4}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <h4>Grupo Directo</h4> 
            <h6>Periodo de Atención en Inicia</h6>
            <table class="table table-striped table-sm thead-light table-bordered dt-responsive nowrap " style="width:100%" id="reporte2">
                <thead>
                    <th>Líder</th>
                    <th>Momento Inicial</th>
                    <th>Momento Real</th>
                    <th>Momento Final</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                    @foreach($lideresIs as $lider)
                        <tr>
                            <td>
                                {{$lider->nombre_lider}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_inicial}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_real}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_final}}
                            </td>
                            <td>
                                <button type="button" class="btn btn-icon btn-sm btn-success trigger--fire-modal-1" id="getEditDiag"  onclick="cargaFrame({{$lider->codigo_lider}})"><i class="fas fa-eye"></i></button>
                                    
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <hr>
            <h6>En Periodo de Atención</h6>
            <table class="table table-striped table-sm thead-light table-bordered dt-responsive nowrap " style="width:100%" id="reporte">
                <thead>
                    <th>Líder</th>
                    <th>Momento Inicial</th>
                    <th>Momento Real</th>
                    <th>Momento Final</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                    @foreach($naturaS as $lider)
                        <tr>
                            <td>
                                {{$lider->nombre_lider}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_inicial}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_real}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_final}}
                            </td>
                            <td>
                                <button type="button" class="btn btn-icon btn-sm btn-success trigger--fire-modal-1" id="getEditDiag"  onclick="cargaFrame({{$lider->codigo_lider}})"><i class="fas fa-eye"></i></button>
                                    
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <h6>Otros líderes</h6>
            <table class="table table-striped table-sm thead-light table-bordered dt-responsive nowrap " style="width:100%" id="reporte">
                <thead>
                    <th>Líder</th>
                    <th>Momento Inicial</th>
                    <th>Momento Real</th>
                    <th>Momento Final</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                    @foreach($lideres as $lider)
                        <tr>
                            <td>
                                {{$lider->nombre_lider}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_inicial}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_real}}
                            </td>
                            <td>
                                {{$lider->momento_carrera_final}}
                            </td>
                            <td>
                                <button type="button" class="btn btn-icon btn-sm btn-success trigger--fire-modal-1" id="getEditDiag"  onclick="cargaFrame({{$lider->codigo_lider}})"><i class="fas fa-eye"></i></button>
                                    
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
</div>
@endsection