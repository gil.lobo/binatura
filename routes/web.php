<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{NaturaController, DashboardController, NaturaGrupoController, LiderController, PermisoController, PorcentajeController, RolController, UsersController};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/login', function () {
    return view('errors.503');
})->name('login');*/
//Route::get('/login', 'errors.503')->name('login');

Route::middleware('auth', 'verified')->group(function () {
	Route::get('/', [DashboardController::class,'index'])->name('welcome');
	Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard');
	Route::view('profile', 'profile')->name('profile');
	Route::get('/file-import',[NaturaController::class,'index'])->name('import-view');
	Route::post('/import',[NaturaController::class,'import'])->name('import');
	Route::get('/file-import-Direct',[NaturaGrupoController::class,'index'])->name('import-view-direct');
	Route::post('/import-Direct',[NaturaGrupoController::class,'import'])->name('import-Direct');
	Route::get('/file-import-Users',[UsersController::class,'indexUser'])->name('import-view-Users');
	Route::post('/import-Users',[UsersController::class,'importUser'])->name('import-Users');
	Route::get('/lideres',[LiderController::class,'index'])->name('lideres');
	Route::get('/lideres/{codigo}',[LiderController::class,'show'])->name('lider.show');
	Route::get('/dashboard/info/{codigo}', [DashboardController::class,'show'])->name('dashboard.show');
	Route::get('/dashboard/infoapp/{id}', [DashboardController::class,'info'])->name('dashboard.info');
	Route::get('/lideres/infoapp/{codigo}', [DashboardController::class,'showLider'])->name('dashboard.showLider');
	Route::prefix('admin')->group(function () {
		Route::get('porcentaje', [PorcentajeController::class,'index'])->name('porcentaje');
		Route::post('porcentaje', [PorcentajeController::class,'store'])->name('porcentaje.store');
		Route::get('porcentaje/show', [PorcentajeController::class,'show'])->name('porcentaje.show');
		Route::post('/uploadimage',[PorcentajeController::class,'import'])->name('uploadimage');
		Route::resource('/usuarios', UsersController::class);
		Route::get('lista', [UsersController::class, 'getUsers'])->name('lista');
		Route::resource('/roles', RolController::class);
		Route::resource('/permisos', PermisoController::class);
	});
	Route::get('sendMessage/', [UsersController::class, 'exp']);
	Route::post('sendMessage/', [UsersController::class, 'export'])->name('send.store');
});
