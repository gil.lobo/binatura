<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Arr;
use DataTables;
use Netflie\WhatsAppCloudApi\WhatsAppCloudApi;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;

class UsersController extends Controller
{
    function __construct()
    {
        $this->middleware('can:ver usuarios')->only('index');
        $this->middleware('can:editar usuarios')->only('edit', 'update');
        $this->middleware('can:crear usuarios')->only('create', 'store');
        //$this->middleware('can:eliminar-usuario', ['only' => ['destroy']]);
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index');
    }

    public function export() 
    {
        $whatsapp_cloud_api = new WhatsAppCloudApi([
            'from_phone_number_id' => '110422415234912',
            'access_token' => 'EAAHfDHRtwB4BAHfsZCWZBdkJEWJzWk4XIjHKxt20YoJKn4bP2BYmqQq3umQgGmOcuOytnC6mtNpnkYr6YEi4OWZA3B1UqtsDsTYWADSzZAXs3PKbQegymfejc6h1wGZAii1cWHD3VNGss5ZB5NPIpE51rYRmZCjcP6I37XEZBlS6alnCRBm8bdM3mjZCxdh7kCITeg0p9RhnRBAZDZD',
        ]);

        $whatsapp_cloud_api->sendTextMessage('527341167212', 'Hey there! I\'m using WhatsApp Cloud API. Visit https://www.netflie.es');
    }

    public function exp()
    {
        return view('welcome');
    }

    public function getUsers(Request $request)
    {
        if ($request->ajax()) {
            $route = 'usuarios.edit';
            $data = User::orderBy('name', 'asc')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="javascript:mex('.$row->id.')" class="edit btn btn-success btn-sm" >Editar</a> <a href="javascript:getDesUser('.$row->id.')" class="delete btn btn-danger btn-sm">Eliminar</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        return view('admin.users.crear', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 
            'email' => 'required|email|unique:users,email',
            'codigo' => ['required', 'integer'],
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRoles = $user->roles->pluck('name', 'name')->all();
        return view('admin.users.edit', compact('user','roles','userRoles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required', 
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);
        $input = $request->all();

        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input, array('password'));
        }

        
        $user = User::find($id);
        $user->update($input);

        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));
        return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            User::find($id)->delete();
            return response()->json([
                'type' => 'success',
                'msg' => 'Usuario eliminado exitosamente'
            ], 201);
        } catch (\Throwable $th) {
            return response()->json([
                'type' => 'error',
                'msg' => 'Ocurrió un error al eliminar'
            ], 422);
        }
        //User::find($id)->delete();
  
        //return redirect()->back();
    }


    //Users imports

    public function indexUser()
    {   
       // $natura = NaturaGrupo::select('ciclo', 'created_at')->orderBy('id', 'desc')->first();
        return view('Imports.users');
    }

    public function importUser(Request $request){
        
        
        try {
            DB::beginTransaction();       
            Excel::import(new UsersImport, $request->file('file'));
            
            DB::commit();
            return redirect()->back()
                ->with('success', 'Excel Subido exitosamente!');
        } catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()
                ->with('error', 'Ocurrió un error intente nuevamente!');
        }       
    }
    

}
