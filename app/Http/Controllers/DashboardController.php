<?php

namespace App\Http\Controllers;

use App\Models\Natura;
use App\Models\NaturaGrupo;
use App\Models\Porcentaje;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $lideresCNE1 = null;
        $lideresCNE2 = null;
        $lideresFN1 = null;
        $lideresFN2 = null;
        $lideresFN3 = null;
        
        $lideresTN1 = null;
        
        $lideresTN2 = null;
        
        $lideresTN3 = null;
        $porscentaje = null;
        $porscentajeRed = null;
        $total = null;
        $totalRed = null;
        //dd();
        if(\Auth::user()->hasAnyRole('Administrador')){
            $natura = Natura::where('codigo_lider', '209446')->orderBy('id', 'desc')->first();
        if($natura){
            $naturaGpo = NaturaGrupo::where('codigo_lider', '209446')->orderBy('id', 'desc')->first();

        $ideal = Porcentaje::where('momento_carrera', $naturaGpo->momento_carrera_inicial)->first();
        //dd($ideal);
         if($natura->momento_carrera_inicial == 'CNE1'){
            $porscentaje = $naturaGpo->disponibles;
            $porscentajeRed = $natura->disponibles;
            $ideal = ['ideal' => 100];
            $ideal = (object)$ideal;
        } elseif($natura->momento_carrera_inicial == 'CNE1'){
            $porscentaje = $naturaGpo->disponibles * .80;
            $porscentajeRed = $natura->disponibles * .80;
            $ideal = ['ideal' => 80];
            $ideal = (object)$ideal;
        }else{
            $porscentaje = ($naturaGpo->disponibles * $ideal->ideal)/100;
            //$porscentaje = $naturaGpo->disponibles * .65;
            $porscentajeRed = ($natura->disponibles * $ideal->ideal)/100;
        }

        $total = $porscentaje - $naturaGpo->activas;
        $totalRed = $porscentajeRed - $natura->activas;
                //morosidad es igual a deuda.
                //CPV = penetración 
        $lideresCNE1= NaturaGrupo::where('momento_carrera_inicial', 'CNE1')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresCNE2= NaturaGrupo::where('momento_carrera_inicial', 'CNE2')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresFN1= NaturaGrupo::where('momento_carrera_inicial', 'FN1')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresFN2= NaturaGrupo::where('momento_carrera_inicial', 'FN2')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresFN3= NaturaGrupo::where('momento_carrera_inicial', 'FN3')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresTN1= NaturaGrupo::where('momento_carrera_inicial', 'TN1')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresTN2= NaturaGrupo::where('momento_carrera_inicial', 'TN2')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresTN3= NaturaGrupo::where('momento_carrera_inicial', 'TN3')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();

        //dd(round($total));
        if ($natura->periodo_antencion == 'SÍ') {
            toastr()->error('Atención Se Encuentra en Periodo de Atención');
        }
        }
        }else{
            $natura = Natura::where('codigo_lider', auth()->user()->codigo)->orderBy('id', 'desc')->first();
        if($natura){
            $naturaGpo = NaturaGrupo::where('codigo_lider', auth()->user()->codigo)->orderBy('id', 'desc')->first();

        $ideal = Porcentaje::where('momento_carrera', $naturaGpo->momento_carrera_inicial)->first();
        //dd($ideal);
         if($natura->momento_carrera_inicial == 'CNE1'){
            $porscentaje = $naturaGpo->disponibles;
            $porscentajeRed = $natura->disponibles;
            $ideal = ['ideal' => 100];
            $ideal = (object)$ideal;
        } elseif($natura->momento_carrera_inicial == 'CNE1'){
            $porscentaje = $naturaGpo->disponibles * .80;
            $porscentajeRed = $natura->disponibles * .80;
            $ideal = ['ideal' => 80];
            $ideal = (object)$ideal;
        }else{
            $porscentaje = ($naturaGpo->disponibles * $ideal->ideal)/100;
            //$porscentaje = $naturaGpo->disponibles * .65;
            $porscentajeRed = ($natura->disponibles * $ideal->ideal)/100;
        }

        $total = $porscentaje - $naturaGpo->activas;
        $totalRed = $porscentajeRed - $natura->activas;
                //morosidad es igual a deuda.
                //CPV = penetración 
        $lideresCNE1= NaturaGrupo::where('momento_carrera_inicial', 'CNE1')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresCNE2= NaturaGrupo::where('momento_carrera_inicial', 'CNE2')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresFN1= NaturaGrupo::where('momento_carrera_inicial', 'FN1')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresFN2= NaturaGrupo::where('momento_carrera_inicial', 'FN2')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresFN3= NaturaGrupo::where('momento_carrera_inicial', 'FN3')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresTN1= NaturaGrupo::where('momento_carrera_inicial', 'TN1')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresTN2= NaturaGrupo::where('momento_carrera_inicial', 'TN2')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();
        $lideresTN3= NaturaGrupo::where('momento_carrera_inicial', 'TN3')->where('codigo_lider_inmediato', auth()->user()->codigo)->orderBy('momento_carrera_inicial', 'asc')->count();

        //dd(round($total));
        if ($natura->periodo_antencion == 'SÍ') {
            toastr()->error('Atención Se Encuentra en Periodo de Atención');
        }
        }
        }
        
        
        return view('dashboard', compact('natura', 'naturaGpo', 'porscentaje', 'total', 'porscentajeRed', 'totalRed',
        'lideresCNE1',
        'lideresCNE2',
        'lideresFN1',
        'lideresFN2',
        'lideresFN3',
        'lideresTN1',
        'lideresTN2',
        'lideresTN3',
        'ideal'));
    }

    public function show($id)
    {
        $INICIA = NaturaGrupo::select('codigo_lider', 'nombre_lider', 'momento_carrera_inicial', 'momento_carrera_real', 'momento_carrera_final')->where('codigo_lider_inmediato', $id)->where('periodo_antencion', 'INICIA')->orderBy('momento_carrera_inicial', 'asc')->count();
        //$INICIA = $lideresIs->count();
        $Si = NaturaGrupo::select('codigo_lider', 'nombre_lider', 'momento_carrera_inicial', 'momento_carrera_real', 'momento_carrera_final')->where('codigo_lider_inmediato', $id)->where('periodo_antencion', 'SÍ')->orderBy('momento_carrera_inicial', 'asc')->count();
        //$Si = $naturaS->count();
        $lideresIs = NaturaGrupo::select('codigo_lider', 'nombre_lider', 'momento_carrera_inicial', 'momento_carrera_real', 'momento_carrera_final', 'periodo_antencion')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->get();
        return view('detalle', compact('lideresIs', 'INICIA', 'Si'));
    }

    public function showLider($codigo)
    {
        $lideresIs = NaturaGrupo::select('codigo_lider', 'nombre_lider', 'momento_carrera_inicial', 'momento_carrera_real', 'momento_carrera_final')->where('periodo_antencion', 'INICIA')->where('codigo_lider_inmediato', $codigo)->orderBy('momento_carrera_inicial', 'asc')->get();
        $INICIA = $lideresIs->count();
        $naturaS = NaturaGrupo::select('codigo_lider', 'nombre_lider', 'momento_carrera_inicial', 'momento_carrera_real', 'momento_carrera_final')->where('periodo_antencion', 'SÍ')->where('codigo_lider_inmediato', $codigo)->orderBy('momento_carrera_inicial', 'asc')->get();
        $Si = $naturaS->count();
        return view('detalle', compact('lideresIs', 'INICIA', 'naturaS', 'Si'));
    }

    public function info($id)
    {
        $lideresFN1 = null;
        $lideresFN21 = null;
        $lideresFN22 = null;
        $lideresFN31 = null;
        $lideresFN32 = null;
        $lideresFN32 = null;
        $lideresTN1 = null;
        $lideresTN12 = null;
        $lideresTN13 = null;
        $lideresTN14 = null;
        $lideresTN21 = null;
        $lideresTN22 = null;
        $lideresTN23 = null;
        $lideresTN24 = null;
        $lideresTN31 = null;
        $lideresTN32 = null;
        $lideresTN33 = null;
        $lideresTN34 = null;
        $natura = NaturaGrupo::where('codigo_lider', $id)->orderBy('id', 'desc')->first();
        $lideresIs = NaturaGrupo::where('periodo_antencion', 'Si')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->get();
        $lideres = NaturaGrupo::where('periodo_antencion', 'NO')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->get();
        $naturaS = NaturaGrupo::select('codigo_lider', 'nombre_lider', 'momento_carrera_inicial', 'momento_carrera_real', 'momento_carrera_final')->where('periodo_antencion', 'Si')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->get();
        if ($natura->momento_carrera_inicial == 'FN1') {
            $lideresFN1= NaturaGrupo::where('momento_carrera_inicial', 'CNE1')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
        } else if ($natura->momento_carrera_inicial == 'FN2'){
            $lideresFN21= NaturaGrupo::where('momento_carrera_inicial', 'CNE2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresFN22= NaturaGrupo::where('momento_carrera_inicial', 'FN1')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();

        } else if ($natura->momento_carrera_inicial == 'FN3'){
            $lideresFN31= NaturaGrupo::where('momento_carrera_inicial', 'CNE2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresFN32= NaturaGrupo::where('momento_carrera_inicial', 'FN1')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresFN32= NaturaGrupo::where('momento_carrera_inicial', 'FN2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();

        }else if ($natura->momento_carrera_inicial == 'TN1'){
            $lideresTN1= NaturaGrupo::where('momento_carrera_inicial', 'CNE2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresTN12= NaturaGrupo::where('momento_carrera_inicial', 'FN1')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresTN13= NaturaGrupo::where('momento_carrera_inicial', 'FN2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresTN14= NaturaGrupo::where('momento_carrera_inicial', 'FN3')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();

        }else if ($natura->momento_carrera_inicial == 'TN2'){
            $lideresTN21= NaturaGrupo::where('momento_carrera_inicial', 'FN1')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresTN22= NaturaGrupo::where('momento_carrera_inicial', 'FN2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresTN23= NaturaGrupo::where('momento_carrera_inicial', 'FN3')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresTN24= NaturaGrupo::where('momento_carrera_inicial', 'TN1')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();

        }else if ($natura->momento_carrera_inicial == 'TN3'){
            $lideresTN31= NaturaGrupo::where('momento_carrera_inicial', 'FN2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresTN32= NaturaGrupo::where('momento_carrera_inicial', 'FN3')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresTN33= NaturaGrupo::where('momento_carrera_inicial', 'TN1')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
            $lideresTN34= NaturaGrupo::where('momento_carrera_inicial', 'TN2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();

        }
        //dd($lideresIsis);
        return view('frames.fram-info', compact('natura', 'lideresIs', 'naturaS', 'lideres', 'lideresFN1', 'lideresFN21', 'lideresFN22', 'lideresFN31', 
        'lideresFN32',
        'lideresFN32',
        'lideresTN1',
        'lideresTN12',
        'lideresTN13',
        'lideresTN14',
        'lideresTN21',
        'lideresTN22',
        'lideresTN23',
        'lideresTN24',
        'lideresTN31',
        'lideresTN32',
        'lideresTN33',
        'lideresTN34'));
    }
}
