<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Porcentaje;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic;

class PorcentajeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.iteraccion.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Porcentaje::truncate();
            Porcentaje::create(['ideal' => $request->input('fn1'), 'momento_carrera' => 'FN1']);
            Porcentaje::create(['ideal' => $request->input('fn2'), 'momento_carrera' => 'FN2']);
            Porcentaje::create(['ideal' => $request->input('fn3'), 'momento_carrera' => 'FN3']);
            Porcentaje::create(['ideal' => $request->input('tn1'), 'momento_carrera' => 'TN1']);
            Porcentaje::create(['ideal' => $request->input('tn2'), 'momento_carrera' => 'TN2']);
            Porcentaje::create(['ideal' => $request->input('tn3'), 'momento_carrera' => 'TN3']);
            Porcentaje::create(['ideal' => $request->input('ins'), 'momento_carrera' => 'INS']);
            Porcentaje::create(['ideal' => $request->input('an'), 'momento_carrera' => 'AN']);
            return redirect()->back()
                ->with('success', 'Información guardada exitosamente!');
        } catch (\Throwable $th) {
            return redirect()->back()
                ->with('error', 'Ocurrió un error intente nuevamente!');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $Porcentaje = Porcentaje::get();
        return response()->json($Porcentaje);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function import(Request $request){
        
        $request->validate([
            'file' => 'required|mimes:jpg,png,jpeg|max:2048',
            'tipo' => 'required'
            
        ]);
        try {
            if($request->hasfile('file')):
                $imagen         = $request->file('file');
                $tipo = $request->input('tipo');
                $nombreimagen   = $tipo."."."jpeg";
                //$nombreimagen = $imagen->getClientOriginalName();
                $ruta           = public_path("stisla/img/");
                $imagen->move($ruta,$nombreimagen);         
                //$model->foto  = $nombreimagen; // asignar el nombre para guardar
            endif;
            return redirect()->back()
                ->with('success', 'Excel Subido exitosamente!');
        } catch (\Exception $e){
            return redirect()->back()
                ->with('error', 'Ocurrió un error intente nuevamente!');
        }  
        
         
    }
}
