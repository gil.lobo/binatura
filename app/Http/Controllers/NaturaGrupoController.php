<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportNaturaGpo;
use App\Models\NaturaGrupo;
use Illuminate\Support\Facades\DB;

class NaturaGrupoController extends Controller
{
    public function index()
    {   
        $natura = NaturaGrupo::select('ciclo', 'created_at')->orderBy('id', 'desc')->first();
        return view('Imports.direct', compact('natura'));
    }

    public function import(Request $request){
        $request->validate([
            'file' => 'required|mimes:xlx,xls,xlsx|max:2048',
            'ciclo' => 'required'
        ]);
        $natura = NaturaGrupo::select('id')->get();
        foreach ($natura as $key) {
            $this->destroy($key['id']);
        }
        try {
            DB::beginTransaction();
            $natura = NaturaGrupo::select('id')->get();
            
            foreach ($natura as $key) {
                $this->destroy($key['id']);
            }
       
            Excel::import(new ImportNaturaGpo, $request->file('file'));
            NaturaGrupo::where('ciclo', null)->update([
                'ciclo' => $request->input('ciclo'),
            ]);
            DB::commit();
            return redirect()->back()
                ->with('success', 'Excel Subido exitosamente!');
        } catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()
                ->with('error', 'Ocurrió un error intente nuevamente!');
        }       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = NaturaGrupo::find($id)->delete();
    }

}
