<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportNatura;
use App\Models\Natura;

class NaturaController extends Controller
{
    public function index()
    {   
        $natura = Natura::select('ciclo', 'created_at')->orderBy('id', 'desc')->first();
        return view('Imports.index', compact('natura'));
    }

    public function import(Request $request){
        $request->validate([
            'file' => 'required|mimes:xlx,xls,xlsx|max:2048',
            'ciclo' => 'required'
        ]);
        $natura = Natura::select('id')->get();
        foreach ($natura as $key) {
            $this->destroy($key['id']);
        }
        try {
            Excel::import(new ImportNatura, $request->file('file'));
            Natura::where('ciclo', null)->update([
                'ciclo' => $request->input('ciclo'),
            ]);
            return redirect()->back()
                ->with('success', 'Excel Subido exitosamente!');
        } catch (\Exception $e){
            return redirect()->back()
                ->with('error', 'Ocurrió un error intente nuevamente!');
        }     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Natura::find($id)->delete();
    }
}
