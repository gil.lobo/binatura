<?php

namespace App\Http\Controllers;

use App\Models\Natura;
use App\Models\NaturaGrupo;
use App\Models\Porcentaje;
use Illuminate\Http\Request;

class LiderController extends Controller
{
    public function index()
    {
        $lideres = null;
        if (auth()->user()->codigo == '209446' || \Auth::user()->hasAnyRole('Administrador')) {
            $natura = Natura::select('ciclo')->where('codigo_lider', '209446')->orderBy('id', 'desc')->first();
            $lideres = Natura::where('ciclo', $natura->ciclo)->orderBy('nombre_lider', 'asc')->get();
        }else{
            $natura = Natura::select('ciclo')->where('codigo_lider', auth()->user()->codigo)->orderBy('id', 'desc')->first();
            $lideres = Natura::where('codigo_lider_inmediato', auth()->user()->codigo)->where('ciclo', $natura->ciclo)->orderBy('nombre_lider', 'asc')->get();
        }
        
        
        return view('Lideres.index', compact('lideres'));
    }

    public function show($id)
    {
        $lideresCNE1 = null;
        $lideresCNE2 = null;
        $lideresFN1 = null;
        $lideresFN2 = null;
        $lideresFN3 = null;
        
        $lideresTN1 = null;
        
        $lideresTN2 = null;
        
        $lideresTN3 = null;
        $natura = Natura::where('codigo_lider', $id)->orderBy('id', 'desc')->first();
        $naturaGpo = NaturaGrupo::where('codigo_lider', $id)->orderBy('id', 'desc')->first();
        $lideres = Natura::where('codigo_lider_inmediato', $id)->where('ciclo', $natura->ciclo)->orderBy('nombre_lider', 'asc')->get();
        $ideal = Porcentaje::where('momento_carrera', $naturaGpo->momento_carrera_inicial)->first();
        //dd($ideal->ideal);
         if($natura->momento_carrera_inicial == 'CNE1'){
            $porscentaje = $naturaGpo->disponibles;
            $porscentajeRed = $natura->disponibles;
            $ideal = ['ideal' => 100];
            $ideal = (object)$ideal;
        } elseif($natura->momento_carrera_inicial == 'CNE2'){
            $porscentaje = $naturaGpo->disponibles * .80;
            $porscentajeRed = $natura->disponibles * .80;
            $ideal = ['ideal' => 80];
            $ideal = (object)$ideal;
        }else{
            $porscentaje = ($naturaGpo->disponibles * $ideal->ideal)/100;
            //$porscentaje = $naturaGpo->disponibles * .65;
            $porscentajeRed = ($natura->disponibles * $ideal->ideal)/100;
        }
        //dd($ideal);
        $total = $porscentaje - $naturaGpo->activas;
                $totalRed = $porscentajeRed - $natura->activas;
                $lideresCNE1= NaturaGrupo::where('momento_carrera_inicial', 'CNE1')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
                $lideresCNE2= NaturaGrupo::where('momento_carrera_inicial', 'CNE2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
                $lideresFN1= NaturaGrupo::where('momento_carrera_inicial', 'FN1')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
                $lideresFN2= NaturaGrupo::where('momento_carrera_inicial', 'FN2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
                $lideresFN3= NaturaGrupo::where('momento_carrera_inicial', 'FN3')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
                $lideresTN1= NaturaGrupo::where('momento_carrera_inicial', 'TN1')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
                $lideresTN2= NaturaGrupo::where('momento_carrera_inicial', 'TN2')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();
                $lideresTN3= NaturaGrupo::where('momento_carrera_inicial', 'TN3')->where('codigo_lider_inmediato', $id)->orderBy('momento_carrera_inicial', 'asc')->count();


        if ($natura->periodo_antencion == 'SÍ') {
            toastr()->error('Atención El Usuario se encuentra en Periodo de Atención');
        }
        return view('Lideres.show', compact('natura', 'naturaGpo', 'lideres', 'porscentaje', 'total', 'porscentajeRed', 'totalRed', 'lideresCNE1', 'lideresCNE2', 'lideresFN1', 'lideresFN2', 
            'lideresFN3',
            'lideresTN1',
            'lideresTN2',
            'lideresTN3',
            'ideal'
        ));
    }
}
