<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Natura extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'codigo_lider',
        'nombre_lider',
        'estatus',
        'momento_carrera_inicial',
        'momento_carrera_final',
        'momento_carrera_real',
        'periodo_antencion',
        'codigo_lider_inmediato',
        'lider_inmediata',
        'total_lideres',
        'disponibles',
        'saldo_disponibles',
        'activas',
        'inactividad1',
        'inactividad2',
        'inactividad3',
        'inactividad4',
        'actividad',
        'morosidad',
        'cpv',
        //'ciclo_inicio',
        //'aniversario2'
    ];
}
