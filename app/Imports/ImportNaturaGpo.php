<?php

namespace App\Imports;

use App\Models\NaturaGrupo;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class ImportNaturaGpo implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $dispo = str_replace(',', '', $row['disponibles']);
        $total_lideres = str_replace(',', '', $row['total_lideres']);
        $activas = str_replace(',', '', $row['activas']);
        $inactiva_1 = str_replace(',', '', $row['inactiva_1']);
        $inactiva_2 = str_replace(',', '', $row['inactiva_2']);
        $inactiva_3 = str_replace(',', '', $row['inactiva_3']);
        $inactiva_4 = str_replace(',', '', $row['inactiva_4']);
        
        $saldo_de_disponibles = str_replace(',', '', $row['saldo_de_disponibles']);

        $fecha = substr($row['aniversario_como_lider'],0,4)."-".substr($row['aniversario_como_lider'],4,5);
        //$newDate = date('Y-m',$fecha);
        //dd($fecha);
        $morosidad = str_replace(',', '', $row['deuda']);
        $cpv = str_replace(',', '', $row['penetracion_creer_para_ver']);
        return new NaturaGrupo([
            'codigo_lider'  => $row['codigo_de_la_lider'],
            'nombre_lider' => $row['lider'],
            'estatus'    => $row['estatus'],
            'momento_carrera_inicial'  => $row['momento_de_carrera_inicial'],
            'momento_carrera_final' => $row['momento_de_carrera_real'],
            'momento_carrera_real'    => $row['momento_de_carrera_final'],
            'periodo_antencion'  => str_replace('SÃ', 'Si', $row['periodo_atencion']),
            'codigo_lider_inmediato' => $row['codigo_de_la_lider_inmediata'],
            'lider_inmediata' => $row['lider_inmediata'],
            'total_lideres'    => intval($row['total_lideres']),
            'disponibles'  => intval($row['disponibles']),
            'saldo_disponibles' => intval($row['saldo_de_disponibles']),
            'activas'    => intval($row['activas']),
            'inactividad1'  => intval($row['inactiva_1']),
            'inactividad2' => intval($row['inactiva_2']),
            'inactividad3'    => intval($row['inactiva_3']),
            'inactividad4'    => intval($row['inactiva_4']),
            'actividad' => $row['actividad'],
            'codigo_lider'  => $row['codigo_de_la_lider'],
            'nombre_lider' => $row['lider'],
            'estatus'    => $row['estatus'],
            'momento_carrera_inicial'  => $row['momento_de_carrera_inicial'],
            'momento_carrera_final' => $row['momento_de_carrera_real'],
            'momento_carrera_real'    => $row['momento_de_carrera_final'],
            'periodo_antencion'  => str_replace('SÃ', 'Si', $row['periodo_atencion']),
            'codigo_lider_inmediato' => $row['codigo_de_la_lider_inmediata'],
            'lider_inmediata' => $row['lider_inmediata'],
            'total_lideres'    => intval($total_lideres),
            'disponibles'  => intval($dispo),
            'saldo_disponibles' => intval($saldo_de_disponibles),
            'activas'    => intval($activas),
            'inactividad1'  => intval($inactiva_1),
            'inactividad2' => intval($inactiva_2),
            'inactividad3'    => intval($inactiva_3),
            'inactividad4'    => intval($inactiva_4),
            'actividad' => $row['actividad'],
            'morosidad' => $morosidad,
            'cpv' => $cpv,
            'ciclo_inicio' => intval($row['ciclos_con_el_mismo_momento_de_carrera']),
            'aniversario2' => $fecha,
        ]);
    }
}
